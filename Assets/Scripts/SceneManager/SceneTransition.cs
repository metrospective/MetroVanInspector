using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransition : MonoBehaviour
{
    // Variable to store the previous scene index
    private static int previousSceneIndex = -1;
    [SerializeField]
    public float FadeOutTime = 2.0f;

    public void GoToPreviousScene()
    {
        if (previousSceneIndex != -1)
        {
            Debug.Log("*****************CHANGING TO PREVIOUS SCENE*************");
            Invoke("LoadPreviousScene", FadeOutTime);
        }
        else
        {
            Debug.Log("No previous scene to go back to.");
        }
    }

    // Method to set the previous scene index
    public void SetPreviousScene()
    {
        previousSceneIndex = SceneManager.GetActiveScene().buildIndex;
        Debug.Log("Previous Scene Index: " + previousSceneIndex);
    }
    
    public void SetPreviousScene(int sceneIndex)
    {
        previousSceneIndex = sceneIndex;
    }

    public void GoToStartScene()
    {
        Debug.Log("*****************CHANGING SCENE*************");
        SetPreviousScene();
        Invoke("LoadStartScene", FadeOutTime);
    }

    public void GoToBoneyard()
    {
        Debug.Log("*****************CHANGING SCENE*************");
        SetPreviousScene();
        Invoke("LoadBoneyard", FadeOutTime);
    }

    public void GoToSeymourLake()
    {
        Debug.Log("*****************CHANGING SCENE*************");
        SetPreviousScene();
        Invoke("LoadSeymour", FadeOutTime);
    }

    public void GoToRiverside()
    {
        Debug.Log("*****************CHANGING SCENE*************");
        SetPreviousScene();
        Invoke("LoadRiverside", FadeOutTime);
    }

    public void GoToAnnacis()
    {
        Debug.Log("*****************CHANGING SCENE*************");
        SetPreviousScene();
        Invoke("LoadAnnacis", FadeOutTime);
    }

    public void GoToBurns()
    {
        Debug.Log("*****************CHANGING SCENE*************");
        SetPreviousScene();
        Invoke("LoadBurns", FadeOutTime);
    }

    public void GoToReworld()
    {
        Debug.Log("*****************CHANGING SCENE*************");
        SetPreviousScene();
        Invoke("LoadReworld", FadeOutTime);
    }

    public void GoToVideoBoneyard()
    {
        SetPreviousScene();
        Invoke("LoadVideoBoneyard", FadeOutTime);
    }

    public void GoToVideoSeymourLake()
    {
        SetPreviousScene();
        Invoke("LoadVideoSeymourLake", FadeOutTime);
    }

    public void GoToVideoRiverside()
    {
        SetPreviousScene();
        Invoke("LoadVideoRiverside", FadeOutTime);
    }

    public void GoToVideoAnnacis()
    {
        SetPreviousScene();
        Invoke("LoadVideoAnnacis", FadeOutTime);
    }

    public void GoToVideoBurns()
    {
        SetPreviousScene();
        Invoke("LoadVideoBurns", FadeOutTime);
    }

    public void GoToVideoReworld()
    {
        SetPreviousScene();
        Invoke("LoadVideoReworld", FadeOutTime);
    }

    public void GoToEndScreen()
    {
        SetPreviousScene();
        Invoke("LoadEndScreen", FadeOutTime);
    }

    void LoadStartScene()
    {
        SceneManager.LoadScene(0);
    }
    void LoadBoneyard()
    {
        SceneManager.LoadScene(1);
    }
    void LoadSeymour()
    {
        SceneManager.LoadScene(2);
    }
    void LoadRiverside()
    {
        SceneManager.LoadScene(3);
    }
    void LoadAnnacis()
    {
        SceneManager.LoadScene(4);
    }
    void LoadBurns()
    {
        SceneManager.LoadScene(5);
    }
    void LoadReworld()
    {
        SceneManager.LoadScene(6);
    } 
    void LoadVideoAnnacis()
    {
        SceneManager.LoadScene(7);
    }
      void LoadVideoBoneyard()
    {
        SceneManager.LoadScene(8);
    }
    void LoadVideoBurns()
    {
        SceneManager.LoadScene(9);
    }
    void LoadVideoReworld()
    {
        SceneManager.LoadScene(10);
    }
       void LoadVideoRiverside()
    {
        SceneManager.LoadScene(11);
    }
    void LoadVideoSeymourLake()
    {
        SceneManager.LoadScene(12);
    }
    void LoadEndScreen()
    {
        SceneManager.LoadScene(13);
    }
    void LoadPreviousScene(){
        SceneManager.LoadScene(previousSceneIndex);
    }
    

}
